--------------------------------------------------------------------------------------------------------------------------
MAIN INFORMATION:  
1. This is standard PSO method which has been created to use MSc Software Engineering dissertation project.  
2. The method will be used for solving shortest path problem later.  
3. There are two different implementation in the project.  
	- with class object  
	- without class object  
4. All conditions were kept same as possibly as it could be.  
	- constants are same  
	- utility functions are same  
	- initialization methods are same  
	- main loops are same  
5. In one project, to represent a particle and to keep the data belongs particle, class object is used  
6. In other project, data belongs to particle simply is stored in arrays  
--------------------------------------------------------------------------------------------------------------------------
PROJECT STEPS:  
1. randomly initialize swarm as a first state: create coordinates and velocities  
2. evaluate each particle's state and calculate fitness values  
3. setting initial state as best known position of each particle  
4. by iterating fitness values list, find best particle and set it as best position of swarm  
5. while(max iteration is not completed and error tolerance is not reached)  
	5.1. calculate inertia coefficient  
	5.2. for (iterate all particles)  
	- randomly calculate R1 and R2  
	- calculate next velocity  
	- control max velocity limit  
	- calculate next position  
	- control feasible search area  
	- if(new position is better than particle's best know position) --> update particle's best known position  
	- find best particle by iterating fitness list  
	- if(any new best position of swarm is better than global best position) --> update swarm's best position so far  
--------------------------------------------------------------------------------------------------------------------------
		
