package with_object;
/*
    - each particle object has location, velocity and fitness values (X and Y coordinates)
    - in defining an object with random initializer, constructor will assign values
    - during the process, getters and setters will work to change the values
*/
public class Particle {
    private double locX;
    private double locY;
    private double velX;
    private double velY;

    public Particle(){}

    public Particle(double locX, double locY, double velX, double velY){
        this.locX = locX;
        this.locY = locY;
        this.velX = velX;
        this.velY= velY;
    }

    public double getLocX() { return locX; }
    public void setLocX(double locX) { this.locX = locX; }
    public double getLocY() { return locY; }
    public void setLocY(double locY) { this.locY = locY; }
    public double getVelX() { return velX; }
    public void setVelX(double velX) { this.velX = velX; }
    public double getVelY() { return velY; }
    public void setVelY(double velY) { this.velY = velY; }
    public double getFitness() { return Problem.regular(this); }
}
