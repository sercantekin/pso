package with_object;

import java.util.Random;

public class Process {
    // list of particles. best particle object. fitness list. best coordinates list
    private static Particle[] swarm = new Particle[Problem.swarm];
    private static Particle gBest = new Particle();
    private static double[] fitness = new double[Problem.swarm];
    private static double[][] pBest = new double[Problem.swarm][Problem.dimension];
    private static Random rnd = new Random();
    private static int bestI;

    // program entry point
    public static void runPSO(){
        double r1, r2, locX, locY, velX, velY, w;
        initializeSwarm();

        int i = 0;
        while (i < Problem.maxIteration && gBest.getFitness() > Problem.errTolerance){
            // inertia coefficient
            w = Problem.wMax - (Problem.wMax - Problem.wMin) / ((double) i / Problem.maxIteration);

            // inner loop to iterate all particles
            for (int j = 0; j < Problem.swarm; j++){
                r1 = rnd.nextDouble();
                r2 = rnd.nextDouble();

                // getting previous location coordinates
                locX = swarm[j].getLocX();
                locY = swarm[j].getLocY();
                velX = swarm[j].getVelX();
                velY = swarm[j].getVelY();

                // calculating next velocity
                velX = velX * w + (r1 * Problem.c1) * (pBest[j][0] - locX) +
                                  (r2 * Problem.c2) * (gBest.getLocX() - locX);
                velY = velY * w + (r1 * Problem.c1) * (pBest[j][1] - locY) +
                                  (r2 * Problem.c2) * (gBest.getLocY() - locY);

                // max velocity control
                velX = Math.min(velX, Problem.vMax);
                velX = Math.max(velX, Problem.vMin);
                velY = Math.min(velY, Problem.vMax);
                velY = Math.max(velY, Problem.vMin);

                // calculating next position
                locX = locX + velX;
                locY = locY + velY;

                // feasible search area control
                locX = Math.min(locX, Problem.xMax);
                locX = Math.max(locX, Problem.xMin);
                locY = Math.min(locY, Problem.yMax);
                locY = Math.max(locY, Problem.yMin);

                // setting new values to particle itself
                swarm[j].setLocX(locX);
                swarm[j].setLocY(locY);
                swarm[j].setVelX(velX);
                swarm[j].setVelY(velY);

                // updating pBest
                if (fitness[j] > swarm[j].getFitness()){
                    pBest[j][0] = locX;
                    pBest[j][1] = locY;
                    fitness[j] = swarm[j].getFitness();
                }

                // updating gBest
                bestI = Utility.besSoFarIndex(fitness);
                if (fitness[bestI] < gBest.getFitness()){
                    gBest.setLocX(locX);
                    gBest.setLocY(locY);
                }
            }
            i++;
            System.out.println("Best particle: " + (bestI + 1) + "\tX: " + gBest.getLocX()
                    + "\tY: " + gBest.getLocY() + "\tFitness: " + gBest.getFitness());
        }
        System.out.println("\nBest particle: " + (bestI + 1) + "\nX: " + gBest.getLocX() +
                "\nY: " + gBest.getLocY() + "\nFitness: " + gBest.getFitness() + "\nIteration: " + i + "\n");
    }

    // randomly initialize swarm as a first state
    private static void initializeSwarm(){
        Particle p;
        double locX, locY, velX, velY;
        for (int i = 0; i < Problem.swarm; i++){
            // randomly creating coordinates and velocities
            locX = Problem.xMin + rnd.nextDouble() * (Problem.xMax - Problem.xMin);
            locY = Problem.yMin + rnd.nextDouble() * (Problem.yMax - Problem.yMin);
            velX = Problem.vMin + rnd.nextDouble() * (Problem.vMax - Problem.vMin);
            velY = Problem.vMin + rnd.nextDouble() * (Problem.vMax - Problem.vMin);

            // creating particle, and adding to list, update fitness list
            p = new Particle(locX, locY, velX, velY);
            swarm[i] = p;
            fitness[i] = p.getFitness();

            // setting initial state as best known position of each particle
            pBest[i][0] = locX;
            pBest[i][1] = locY;
        }

        // finding best particle after initializing and setting values
        bestI = Utility.besSoFarIndex(fitness);
        gBest.setLocX(swarm[bestI].getLocX());
        gBest.setLocY(swarm[bestI].getLocY());
    }
}
