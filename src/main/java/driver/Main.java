package driver;

import with_object.Process;
import without_object.Process_;
/*
	- problem to be solved: find x and y that minimize the function below:
		f(x, y) = (2.8125 - x + x * y^4)^2 + (2.25 - x + x * y^2)^2 + (1.5 - x + x*y)^2
		where 1 <= x <= 4, and -1 <= y <= 1

	- this is for only 2d problems because later this method will be implemented for shortest path problem in 2d graph

	- there are two different methods:
		1. with_object: each particle is a class object in which there are coordinates, velocities and fitness
		2. without_object: each particle's information is stored in arrays, there is no object
*/
public class Main {
	public static void main(String[] args) {
		// running both method
		long startTime = System.nanoTime();
		Process.runPSO();
		long midTime   = System.nanoTime();
		Process_.runPSO();
		long endTime   = System.nanoTime();

		// printing run time of both method
		System.out.println("\nDuration with object: " + (midTime - startTime) / 1000000000.0 + " seconds");
		System.out.println("Duration without object: " + (endTime - midTime) / 1000000000.0 + " seconds");
	}
}
