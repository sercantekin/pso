package without_object;

import java.util.Random;

public class Process_ {
    // without objects, all information will be stored in arrays
    private static double[][] locations = new double[Problem_.swarm][Problem_.dimension];
    private static double[][] velocities = new double[Problem_.swarm][Problem_.dimension];
    private static double[][] pBest = new double[Problem_.swarm][Problem_.dimension];
    private static double[] gBest = new double[Problem_.dimension];
    private static double[] fitness = new double[Problem_.swarm];
    private static Random rnd = new Random();
    private static int bestI;

    // program entry point
    public static void runPSO(){
        double r1, r2, locX, locY, velX, velY, w;
        initializeSwarm();

        int i = 0;
        while (i < Problem_.maxIteration && Problem_.regular(gBest[0], gBest[1]) > Problem_.errTolerance){
            // inertia coefficient
            w = Problem_.wMax - (Problem_.wMax - Problem_.wMin) / ((double) i / Problem_.maxIteration);

            // inner loop to iterate all particles
            for (int j = 0; j < Problem_.swarm; j++){
                r1 = rnd.nextDouble();
                r2 = rnd.nextDouble();

                // getting previous location coordinates
                locX = locations[j][0];
                locY = locations[j][1];
                velX = velocities[j][0];
                velY = velocities[j][1];

                // calculating next velocity
                velX = velX * w + (r1 * Problem_.c1) * (pBest[j][0] - locX) +
                                  (r2 * Problem_.c2) * (gBest[0] - locX);
                velY = velY * w + (r1 * Problem_.c1) * (pBest[j][1] - locY) +
                                  (r2 * Problem_.c2) * (gBest[1] - locY);

                // max velocity control
                velX = Math.min(velX, Problem_.vMax);
                velX = Math.max(velX, Problem_.vMin);
                velY = Math.min(velY, Problem_.vMax);
                velY = Math.max(velY, Problem_.vMin);

                // calculating next position
                locX = locX + velX;
                locY = locY + velY;

                // feasible search area control
                locX = Math.min(locX, Problem_.xMax);
                locX = Math.max(locX, Problem_.xMin);
                locY = Math.min(locY, Problem_.yMax);
                locY = Math.max(locY, Problem_.yMin);

                // setting new values to particle itself
                locations[j][0] = locX;
                locations[j][1] = locY;
                velocities[j][0] = velX;
                velocities[j][1] = velY;

                // updating pBest
                if (fitness[j] > Problem_.regular(locX, locY)){
                    pBest[j][0] = locX;
                    pBest[j][1] = locY;
                    fitness[j] = Problem_.regular(locX, locY);
                }

                // updating gBest
                bestI = Utility_.besSoFarIndex(fitness);
                if (fitness[bestI] < Problem_.regular(gBest[0], gBest[1])){
                    gBest[0] = locX;
                    gBest[1] = locY;
                }
            }
            i++;
            System.out.println("Best particle: " + (bestI + 1) + "\tX: " + gBest[0]
                    + "\tY: " + gBest[1] + "\tFitness: " + Problem_.regular(gBest[0], gBest[1]));
        }
        System.out.println("\nBest particle: " + (bestI + 1) + "\nX: " + gBest[0] +
                "\nY: " + gBest[1] + "\nFitness: " + Problem_.regular(gBest[0], gBest[1]) + "\nIteration: " + i);
    }

    // randomly initialize swarm as a first state
    private static void initializeSwarm(){
        double locX, locY, velX, velY;
        for (int i = 0; i < Problem_.swarm; i++){
            // randomly creating coordinates and velocities
            locX = Problem_.xMin + rnd.nextDouble() * (Problem_.xMax - Problem_.xMin);
            locY = Problem_.yMin + rnd.nextDouble() * (Problem_.yMax - Problem_.yMin);
            velX = Problem_.vMin + rnd.nextDouble() * (Problem_.vMax - Problem_.vMin);
            velY = Problem_.vMin + rnd.nextDouble() * (Problem_.vMax - Problem_.vMin);

            // storing coordinates, velocities. and calculate fitness
            locations[i][0] = locX;
            locations[i][1] = locY;
            velocities[i][0] = velX;
            velocities[i][1] = velY;
            fitness[i] = Problem_.regular(locX, locY);

            // setting initial state as best known position of each particle
            pBest[i][0] = locX;
            pBest[i][1] = locY;
        }

        // finding best particle after initializing and setting values
        bestI = Utility_.besSoFarIndex(fitness);
        gBest[0] = locations[bestI][0];
        gBest[1] = locations[bestI][1];
    }
}
