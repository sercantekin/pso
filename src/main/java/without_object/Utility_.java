package without_object;

public class Utility_ {
    // to find smaller fitness index in fitness list of swarm
    public static int besSoFarIndex(double[] list){
        int min = 0;

        for (int i = 0; i < list.length; i++){
            if (list[i] < list[min]) min = i;
        }
        return min;
    }
}
