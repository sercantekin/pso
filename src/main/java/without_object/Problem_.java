package without_object;

public class Problem_ {
    // constants
    public static final int dimension = 2;
    public static final int maxIteration = 100;
    public static final int swarm = 30;
    public static final double c1 = 0.8;
    public static final double c2 = 0.9;
    public static final double wMax = 0.9;
    public static final double wMin = 0.5;
    public static final double xMax = 4.0;
    public static final double xMin = 1.0;
    public static final double yMax = 1.0;
    public static final double yMin = -1.0;
    public static final double vMax = 1.0;
    public static final double vMin = -1.0;
    public static final double errTolerance = 1E-20; // this will be one of the termination condition

    // fitness functions. more can be added here
    public static double regular(double x, double y){
        return Math.pow(2.8125 - x + x * Math.pow(y, 4), 2) + Math.pow(2.25 - x + x * Math.pow(y, 2), 2) + Math.pow(1.5 - x + x * y, 2);
    }
}
